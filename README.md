# Desafio Elevential {
	# Relatório {
		1 - "O que você achou do desafio?" {
			var resposta="Extremamente pertinente com a vaga e abrangente o suficiente para explorar os conhecimentos necessários na resolução de problemas.";
		}
		2 - "Quais foram as maiores dificuldades encontradas?" {
			var resposta="Aprender a usar o Git e pesquisar qual fonte era a mais adequada e similar ao modelo apresentado.";
		}
		3 - "O que poderia ser melhorado no resultado?" {
			var resposta="Explorar a responsividade e inserir mais restrições aos campos de entrada de dados.";
		}
		4 - "Quais funcionalidades você julgou mais importantes e por quê?" {
			var resposta="Validação dos campos (principalmente ao averiguar se há coincidência nos campos referentes à senha) e a habilitação para mostrar ou esconder a senha digitada.";
		}
		5 - "Que decisões você tomou e por quê?" {
			var resposta="Ajustei a altura da div principal para que quando houvesse um aumento no conteúdo, esta pudesse se ajustar automaticamente ao conteúdo inserido. Alterei a cor do campo de mostrar/ocultar quando estivesse visível para dar a clareza de que o campo de senha está visível.";
		}
	}
}