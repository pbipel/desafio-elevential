function rmv(x) {
	document.getElementById(x).remove();
	document.getElementById('camp-1').remove();
}

function showpass(x,y) {
	x=document.getElementById(x);
	y=document.getElementById(y);
	y.type = y.type=='password' ? 'text' : 'password';
	x.innerHTML = y.type=='password' ? 'Show password' : 'Hide password';
	x.className = y.type=='password' ? 'label-pass' : 'label-pass label-pass-clicked';
}

function verifica() {
	var x1=document.getElementById('password');
	var x2=document.getElementById('c_password');
	document.getElementById('camp-1').innerHTML="";
	document.getElementById('camp-1').removeAttribute('class');
	if (x1.value!=x2.value){
		x1.setAttribute("class", "input-error");
		x2.setAttribute("class", "input-error");
		document.getElementById('camp-2').innerHTML="<label>As senhas não correspondem</label>";
		document.getElementById('camp-2').setAttribute("class", "label-input-error");
		x1.focus();
	}else{
		x1.removeAttribute("class");
		x2.removeAttribute("class");
		document.getElementById('camp-2').innerHTML="";
		document.getElementById('camp-2').removeAttribute("class");
		document.getElementById('camp-1').innerHTML="<div class=\"respos respostapos\">Cadastro realizado com sucesso.</div>";
		document.getElementById('camp-1').setAttribute("class", "resposta");
	}
	return false;
}